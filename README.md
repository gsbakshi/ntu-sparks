# NTU Sparks

Language :- Dart
Front-end Framework :- Flutter
Back-end Framework :- Laravel
Scope :- This is a hybrid application (build on IOS and Android), which will facilitate user to to attend classes , buy credits , purchase products and many more.
We have different types of roles which are defined as follows:
-> user
-> admin
This application has below mentioned features:

Gym classes are bifurcated among multiple categories.
Chat functionality has been implemented between User and Admin, in order to clear out any doubts regarding programs.
Profile module will contain basic information, profile and all these details can be edited.
Dashboard will contain a list of all the booked classes, programs and products.
User could browse through these programs in order to get more insights of classes like date,time, duration, cost etc.
User have the rights to mark attendance through QR code within the defined time frame.
Timely notification will be received for booking, cancelation, up-comming class etc.
Online Payment: User can make a payment online to buy credits and that credits will have lifetime value.
Have multiple packages to by credit.
Credit History: It will display all the credit or debit transaction done by the user.
Have accessibility to browse through the product details, check its price, venu, slot, description and buy that particular product.
On attending any class, user will be rewarded with loyalty points, which can be used to buy products or to book further classes.
Order History: Order History of all are products are depicted here.
Users can select classes corresponding to their level, as we have multiple levels as follows:

Level 1
Level 2
Level 3
Level 4



Backend

Backend is handled by Admin, who is responsible for the below mentioned tasks:
Admin is responsible for the creation of category and then the classes belongs to that category.
Reserved classes: Reserved class list is shown along with the customer details, price etc.
Products has to be uploaded by Admin.
Credit packages are creation is the responsibility of Admin.
Venu list could be managed by Admin.
Date wise slots creation.
Admin has access to User list and their basic information.
Order History: It will contain a list of all the orders with their status, customer details etc.
Coupon creation process is also handled by Admin.
Have rights to upload banners, which are visible on Application End.
Transaction list is displayed, having Admin commission and amount details.
Enquiry requests has to be answered by Admin.

Test credential :
Email - live97@yopmail.com
Pwd - 12345678

NOTE :Please contact for further insights.
